const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')

const isDev = process.env.NODE_ENV === 'development'
const isProd = !isDev

console.log('isDev', isDev)

const optimization = () => {
   const config = {   // нужно чтобы не билдить несколько раз одну и ту же либу (например jquery, который и в main.js и в analytics.js)
      splitChunks: {
         chunks: 'all'
      }
   }

   if (isProd) config.minimizer = [
      new OptimizeCssAssetsWebpackPlugin(),
      new TerserWebpackPlugin()
   ]
   return config
}

const filename = ext => isDev ? `[name].${ext}` : `[name].[contenthash].${ext}`

const babelPresets = (preset) => {
   const presets = [
      '@babel/preset-env', // для поддержки es
   ]

   if (preset) presets.push(preset)

   return presets
}

const jsLoaders = (preset) => {
   const loaders = [{
      loader: 'babel-loader',
      options: {
         presets: babelPresets(preset),
      },
   }]

   if (isDev) {
      loaders.push('eslint-loader')
   }

   return loaders
}

module.exports = {
   mode: process.env.NODE_ENV,
   context: path.resolve(__dirname, 'src'),  // общий путь (чтоб постоянно не указывать)
   entry: {    // входные точки
      main: ['@babel/polyfill', './index.jsx'],
      analytics: './analytics.ts'
   },
   output: {   // куда собирать билд и в каком формате
      filename: filename('js'),
      path: path.resolve(__dirname, 'dist')
   },
   resolve: {
      extensions: ['.js', '.json'],    // расширения, которые можно не указывать при импортах
      alias: {    // для удобства импортов
         '@': path.resolve(__dirname, 'src'),
         '@models': path.resolve(__dirname, 'src/models'),
      }
   },
   optimization: optimization(),
   devServer: {   // хот-релоад приложения при сохранении изменений
      port: 4200,
      open: true,
      hot: isDev  // только в dev режиме
   },
   // devtool: isDev ? 'source-map' : '',    // нужно, чтобы в devtools браузера во вкладке source можно было посмотреть исходный, нескомпилированный код
   plugins: [
      new CleanWebpackPlugin(),  // по умолчанию очищает папку из output при каждом билде
      new HTMLWebpackPlugin({    // для работы с HTML
         template: './index.html',
         minify: {
            collapseWhitespace: isProd    // минифицирует html
         }
      }),
      new CopyWebpackPlugin({
         patterns: [
            {
               from: path.resolve(__dirname, 'src/assets/favicon.ico'),
               to: path.resolve(__dirname, 'dist')
            }
         ]
      }),
      new MiniCssExtractPlugin({
         filename: filename('css')
      })
   ],
   module: {   // лоадеры для работы с препроцессорами, картинками и разными файлами
      rules: [
         {
            test: /\.s[ac]ss$/i,
            use: [
               // 'style-loader' // добавит стили в html (Не юзать с MiniCssExtractPlugin)
               MiniCssExtractPlugin.loader,    // вынесет стили в отдельный файл
               'css-loader',
               'sass-loader'  // обрабатывает sass
            ]
         },
         {
            test: /\.(png|jpg|svg|gif)$/,
            use: ['file-loader']
         },
         {
            test: /\.(ttf|woff|woff2|eot)$/,
            use: ['file-loader']
         },
         {  // для работы с js, ts
            test: /\.m?[jt]s$/,
            exclude: /node_modules/,  // не обрабатывать папку node_modules
            use: jsLoaders('@babel/preset-typescript'), // для поддержки ts

         },
         {
            test: /\.m?jsx$/,
            exclude: /node_modules/,  // не обрабатывать папку node_modules
            use: jsLoaders('@babel/preset-react') // для поддержки jsx (React.js)
         }
      ]
   }
}

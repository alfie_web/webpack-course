import * as $ from 'jquery'

const eslintTest = 0
console.log(eslintTest)

function createAnalytics() {
   let counter = 0
   let isDestroyed = false

   // const listener = (): number => counter++
   const listener = () => counter++

   $(document).on('click', listener)

   return {
      restroy() {
         $(document).off('click', listener)
         isDestroyed = true
      },

      getClicks() {
         if (isDestroyed) return `Analytics is destroyed. Total clicks count ${counter}`
         return counter
      }
   }
}

window['analytics'] = createAnalytics()

// import * as $ from 'jquery'
// import Post from '@models/Post'
import React from 'react'
import { render } from 'react-dom'

// import img from './assets/cat.jpg'
// import json from './assets/test'
import './styles/main.scss'
import './babel'

// const post = new Post('Webpack post title', img)
// $('pre').addClass('code').html(post.toString())

const App = () => (
   <div className="App">
      <div className="container">
         <h1>Webpack course!!!!</h1>

         <div className="logo"></div>

         {/* <pre></pre> */}
      </div>
   </div>
)

render(<App />, document.getElementById('app'))

// console.log('Post to string', post.toString())
// console.log('json', json)
